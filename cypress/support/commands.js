/**
 * Long click on the DOM element resulting from {elementGetter} function for the time specified in {holdTime}
 * @param {function} elementGetter - function to get the DOM element to be fired a long click
 * @param {number} holdTime - total time to hold the click
 * @returns the DOM element trigged
 */
Cypress.Commands.add('longClickOn', (elementGetter, holdTime = 500) => {
    //The event is only triggered by hammerjs if "pointerType == 'touch'" or "button === 0" (see the logic in hammer.js:892)
    const hammerJsEventConfig = { pointerType: 'mouse', button: 0, }
    elementGetter().trigger('pointerdown', hammerJsEventConfig);
    cy.wait(holdTime);
    return elementGetter().trigger('pointerup', hammerJsEventConfig);
  });
  