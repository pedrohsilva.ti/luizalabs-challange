beforeEach(() => {
  cy.visit('/choskim/pen/RLYebL');
});

it('expand square and check its dimensions', () => {
  cy.longClickOn(() => cy.get('iframe[id=result]').its('0.contentDocument').its('body').find('.square'))
  .should('have.css', 'height', '225px')
  .should('have.css', 'width', '225px');
});