# luizalabs challenge

this project has a single test to check if a square is expanded after a 500 ms long click

prerequisites for running the project:
 * node-8 or above

## Install dependencies
```npm install```

## Running in headless mode
```npm run cy:run```

## Running in GUI mode
```npm run cy:open```
after gui opens click on challenge.spec.js
